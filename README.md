# Sandbox Deploy

## Prerequisite

- Ansible

## How to run

1. Prepare you inventory file, for example, if you want to deploy to localhost.
    ```
    localhost ansible_connection=local 
    ```
2. Execute this command.
    ```
    ansible-playbook -i <inventory file> --become -K sandbox.yml
    ```
    The `--become` and `-K` options is added because I need to create directory under `/srv`.
    You can put these information into inventory file.
